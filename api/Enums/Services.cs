﻿using System;
namespace monis.api
{
    public enum Services
    {
        Login,
        EvaluateTransaction,
        ExecuteTransaction,
        CatalogCurrency,
        CatalogTransactionType
    }
}
