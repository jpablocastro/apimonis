﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;

namespace monis.api
{
    public static class RequestCreator
    {
        public static T Make<T>(Services TargetService, IDictionary<String, Object> Params = null) where T : Request
        {
            try
            {

                var ServicesDefinition = LoadServiceDefinition();

                switch (TargetService)
                {
                    case Services.Login:
                        var Login = ServicesDefinition.FirstOrDefault(p => p.Name == "login");
                        return new LoginRequest()
                        {
                            Service = Login,
                            Client = (String)Params[parameters.Client],
                            Password = (String)Params[parameters.Password],
                            ChannelID = 1,
                            DeviceID = Guid.NewGuid().ToString(),
                            Timestamp = (long)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds
                        } as T;
                    case Services.CatalogCurrency:
                        return new Request()
                        {
                            Service = ServicesDefinition.FirstOrDefault(p => p.Name == "catalogs_currencies"),
                        } as T;
                    case Services.CatalogTransactionType:
                        return new Request()
                        {
                            Service = ServicesDefinition.FirstOrDefault(p => p.Name == "catalogs_transaction_types"),
                        } as T;
                    case Services.EvaluateTransaction:
                        return new TransactionRequest()
                        {
                            Service = ServicesDefinition.FirstOrDefault(p => p.Name == "evaluate_transaction"),
                            Source = (Source)Params[parameters.Source],
                            Target = (Target)Params[parameters.Target],
                            ReferenceNumber = Guid.NewGuid().ToString(),
                            Description = (String)Params[parameters.Description],
                            CurrencyId = (int)Params[parameters.CurrencyId],
                            Amount = (String)Params[parameters.Amount],
                            TransactionTypeId = (int)Params[parameters.TransactionTypeId],
                            ChannelID = 1,
                            DeviceID = Guid.NewGuid().ToString(),
                            Timestamp = (long)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds
                        } as T;

                    default:
                        throw new Exception("no-matching-service-found");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static ObservableCollection<Service> LoadServiceDefinition()
        {
            try
            {
                string json = string.Empty;
                using (StreamReader r = new StreamReader("../api/json/all_services.json"))
                {

                    json = r.ReadToEnd();
                    return JsonConvert.DeserializeObject<ObservableCollection<Service>>(json);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
