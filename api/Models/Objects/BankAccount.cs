﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class BankAccount
    {
        [JsonProperty(PropertyName = parameters.Number)]
        public String Number { get; set; }

        [JsonProperty(PropertyName = parameters.CurrencyId)]
        public int CurrencyId { get; set; }

        [JsonProperty(PropertyName = parameters.Currency)]
        public Currency Currency { get; set; }

    }
}
