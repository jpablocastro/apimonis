﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class Item
    {
        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "key")]
        public String Key { get; set; }

        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }
    }
}
