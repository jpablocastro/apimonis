﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class LoginData
    {
        [JsonProperty(PropertyName = parameters.Account)]
        public Account Account { get; set; }

    }
}
