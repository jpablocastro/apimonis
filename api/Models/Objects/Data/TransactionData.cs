﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace monis.api
{
    public class TransactionData
    {
        [JsonProperty(PropertyName = parameters.Subjects)]
        public Object Subjects { get; set; }

        [JsonProperty(PropertyName = parameters.Transaction)]
        public Object Transaction { get; set; }

    }
}
