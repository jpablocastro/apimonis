﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class Request
    {
        [JsonIgnore]
        public Service Service { get; set; }

        [JsonProperty(PropertyName = parameters.ChannelId)]
        public int ChannelID { get; set; }

        [JsonProperty(PropertyName = parameters.DeviceId)]
        public String DeviceID { get; set; }

        [JsonProperty(PropertyName = parameters.Timestamp)]
        public long Timestamp { get; set; }
    }
}
