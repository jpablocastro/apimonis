﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class LoginRequest : Request
    {
        [JsonProperty(PropertyName = monis.api.parameters.Client, Required = Required.Always)]
        public String Client { get; set; }

        [JsonProperty(PropertyName = monis.api.parameters.Password, Required = Required.Always)]
        public String Password { get; set; }
    }
}
