﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class TransactionRequest : Request
    {
        [JsonProperty(PropertyName = parameters.Source, Required = Required.Always)]
        public Source Source { get; set; }

        [JsonProperty(PropertyName = parameters.Target, Required = Required.Always)]
        public Target Target { get; set; }

        [JsonProperty(PropertyName = parameters.ReferenceNumber, Required = Required.Always)]
        public String ReferenceNumber { get; set; }

        [JsonProperty(PropertyName = parameters.Description, Required = Required.Default)]
        public String Description { get; set; }

        [JsonProperty(PropertyName = parameters.TransactionTypeId, Required = Required.Always)]
        public int TransactionTypeId { get; set; }

        [JsonProperty(PropertyName = parameters.CurrencyId, Required = Required.Always)]
        public int CurrencyId { get; set; }

        [JsonProperty(PropertyName = parameters.Amount, Required = Required.Always)]
        public String Amount { get; set; }
    }
}
