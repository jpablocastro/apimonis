﻿using System;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Linq;

namespace monis.api
{
    public static class Monis
    {

        public static async Task<T> CallAsync<T>(HttpClient Client, Request Request, String AuthToken = null) where T : Response
        {
            HttpRequestMessage CallRequest = null;
            HttpResponseMessage CallResponse = null;
            try
            {
                CallRequest = MakeRequest(Request, AuthToken);
                CallResponse = await Client.SendAsync(CallRequest);
                CallResponse.EnsureSuccessStatusCode();
                return await ParseAsync<T>(CallResponse);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                if (CallResponse != null)
                {
                    Console.WriteLine("Response");
                    Console.WriteLine("Status:" + CallResponse.StatusCode);
                    if (CallResponse.Content != null)
                    {
                        Console.WriteLine(await CallResponse.Content.ReadAsStringAsync());
                    }
                }
                throw ex;
            }
        }

        internal static HttpRequestMessage MakeRequest(Request Request, String AuthToken = null)
        {
            try
            {
                HttpRequestMessage HTTPRequest = new HttpRequestMessage(Request.Service.Method, Request.Service.Url);
                HTTPRequest = AddAuthorizationHeader(HTTPRequest, Request.Service.AuthorizationBehavior, AuthToken);
                HTTPRequest = AddRequestContent(HTTPRequest, Request);

                Console.WriteLine("***************************************************************************");
                Console.WriteLine("* Mensaje de solicitud: " + Request.Service.Name);
                Console.WriteLine("* URL: " + Request.Service.Url);
                Console.WriteLine("* Headers:");
                Console.WriteLine(HTTPRequest.Headers.ToString());
                if (HTTPRequest.Content != null)
                {
                    Console.WriteLine("* Contenido: ");
                    Console.WriteLine(JsonConvert.SerializeObject(Request, Formatting.Indented));
                }

                return HTTPRequest;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static HttpRequestMessage AddAuthorizationHeader(HttpRequestMessage Request, Authorization Behavior, String AuthToken = null)
        {
            try
            {
                if (!String.IsNullOrEmpty(AuthToken))
                {
                    switch (Behavior)
                    {
                        case Authorization.SendOnly:
                        case Authorization.SendAndReceive:
                            Request.Headers.Add("authorization", AuthToken);
                            return Request;
                        default:
                            return Request;
                    }
                }
                return Request;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static HttpRequestMessage AddRequestContent(HttpRequestMessage HTTPRequest, Request Request)
        {
            try
            {
                switch (Request.Service.Method.Method)
                {
                    case "GET":
                        return HTTPRequest;
                    case "POST":
                    default:
                        HTTPRequest.Content = new StringContent(JsonConvert.SerializeObject(Request), System.Text.Encoding.UTF8, "application/json");
                        return HTTPRequest;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static async Task<T> ParseAsync<T>(HttpResponseMessage Response) where T : Response
        {
            try
            {
                Console.WriteLine("Response");
                Console.WriteLine("Status:" + Response.StatusCode);
                if (Response.Content != null)
                {
                    var Object = JsonConvert.DeserializeObject<T>(await Response.Content.ReadAsStringAsync());
                    Console.WriteLine(JsonConvert.SerializeObject(Object, Formatting.Indented));
                }
                var ParsedResponse = JsonConvert.DeserializeObject<T>(await Response.Content.ReadAsStringAsync());
                ParsedResponse.AuthorizationToken = RecoverAuthorizationToken(Response);
                return ParsedResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static String RecoverAuthorizationToken(HttpResponseMessage Response)
        {
            var AuthorizationToken = String.Empty;
            try
            {
                for (int i = 0; i < Response.Headers.Count(); ++i)
                {
                    if (Response.Headers.ElementAt(i).Key == "Authorization")
                    {
                        AuthorizationToken = Response.Headers.ElementAt(i).Value.First();
                    }
                }

                return AuthorizationToken;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static async Task<ObservableCollection<T>> ParseCatalogAsync<T>(HttpResponseMessage Response) where T : Item
        {
            try
            {
                Console.WriteLine("Response");
                Console.WriteLine("Status:" + Response.StatusCode);
                if (Response.Content != null)
                {
                    var Object = JsonConvert.DeserializeObject<ObservableCollection<T>>(await Response.Content.ReadAsStringAsync());
                    Console.WriteLine(JsonConvert.SerializeObject(Object, Formatting.Indented));
                }
                return JsonConvert.DeserializeObject<ObservableCollection<T>>(await Response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<ObservableCollection<T>> Query<T>(HttpClient Client, Request Request) where T : Item
        {
            HttpRequestMessage CallRequest = null;
            HttpResponseMessage CallResponse = null;
            try
            {
                CallRequest = MakeRequest(Request, AuthToken: null);
                CallResponse = await Client.SendAsync(CallRequest);
                CallResponse.EnsureSuccessStatusCode();
                return await ParseCatalogAsync<T>(CallResponse);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                if (CallResponse != null)
                {
                    Console.WriteLine("Response");
                    Console.WriteLine("Status:" + CallResponse.StatusCode);
                    if (CallResponse.Content != null)
                    {
                        Console.WriteLine(await CallResponse.Content.ReadAsStringAsync());
                    }
                }
                throw ex;
            }
        }
    }
}
